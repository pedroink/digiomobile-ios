//
//  ProductsResult.swift
//  digio
//
//  Created by MacBook Pro 2016 15" on 19/11/20.
//

import Foundation

class ProductsResult: Codable {
    
    var spotlight:[Spotlight]
    var products:[Product] 
    var cash:Cash
    
}
