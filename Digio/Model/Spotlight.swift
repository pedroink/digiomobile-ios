//
//  Spotlight.swift
//  digio
//
//  Created by MacBook Pro 2016 15" on 19/11/20.
//

import Foundation

class Spotlight: Identifiable, Codable {
    
    var name:String
    var bannerURL:String
    var description:String
    
}
