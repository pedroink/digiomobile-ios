//
//  Product.swift
//  digio
//
//  Created by MacBook Pro 2016 15" on 19/11/20.
//

import Foundation

class Product: Identifiable, Codable {
    
    let id = UUID()
    
    var name:String
    var imageURL:String
    var description:String
    
}
