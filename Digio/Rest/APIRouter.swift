//
//  APIRouter.swift
//  digio
//
//  Created by MacBook Pro 2016 15" on 19/11/20.
//

import Foundation
import Alamofire

enum APIRouter: APIConfiguration {
    
    case products
    
    // MARK: - HTTPMethod
    internal var method: HTTPMethod {
        switch self {
            
        case .products:
            return .get
       
        }
    }
    
    // MARK: - Path
    internal var path: String {
        switch self {
            
        case .products:
            return "/sandbox/products"
            
        }
    }
    
    // MARK: - Parameters
    internal var parameters: Parameters? {
        switch self {
            
        case .products:
            return nil
            
        }
        
    }
    
    // MARK: - URLRequestConvertible
    func asURLRequest() throws -> URLRequest {
        let url = try K.ProductionServer.baseURL.asURL()
        
        var urlRequest = URLRequest(url: url.appendingPathComponent(path))
        
        // HTTP Method
        urlRequest.httpMethod = method.rawValue
        
        // Common Headers
        urlRequest.setValue(ContentType.json.rawValue, forHTTPHeaderField: HTTPHeaderField.acceptType.rawValue)
        urlRequest.setValue(ContentType.json.rawValue, forHTTPHeaderField: HTTPHeaderField.contentType.rawValue)
 
        // Parameters
        if let parameters = parameters {
            do {
                urlRequest.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: [])
            } catch {
                throw AFError.parameterEncodingFailed(reason: .jsonEncodingFailed(error: error))
            }
        }
        
        return urlRequest
    }
    
    
}
