//
//  APIConfiguration.swift
//  digio
//
//  Created by MacBook Pro 2016 15" on 19/11/20.
//

import Alamofire

protocol APIConfiguration: URLRequestConvertible {
    var method: HTTPMethod { get }
    var path: String { get }
    var parameters: Parameters? { get }
}

