//
//  K.swift
//  digio
//
//  Created by MacBook Pro 2016 15" on 19/11/20.
//

import Foundation

struct K {
    struct ProductionServer {
        static let baseURL = "https://7hgi9vtkdc.execute-api.sa-east-1.amazonaws.com"
    }
}

enum HTTPHeaderField: String {
    case authentication = "Authorization"
    case contentType = "Content-Type"
    case acceptType = "Accept"
    case acceptEncoding = "Accept-Encoding"
}

enum ContentType: String {
    case json = "application/json"
}
