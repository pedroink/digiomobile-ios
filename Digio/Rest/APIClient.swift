//
//  APIClient.swift
//  digio
//
//  Created by MacBook Pro 2016 15" on 19/11/20.
//

import Alamofire

class APIClient {
    
    static func products( completion:@escaping (AFResult<ProductsResult>)->Void){
        
        AF.request(APIRouter.products).responseDecodable { (response: AFDataResponse<ProductsResult>) in
            completion(response.result)
        }
        
    }
    
}
