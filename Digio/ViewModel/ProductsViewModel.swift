//
//  ProductsViewModel.swift
//  digio
//
//  Created by MacBook Pro 2016 15" on 19/11/20.
//

import NotificationBannerSwift

class ProductsViewModel: ObservableObject {
    
    @Published var isLoading = true
    @Published var products:ProductsResult?
    
    func fetch(){
        isLoading = true
        
        APIClient.products(){ result in
            
            
            
            switch result {
            case .success(let result):
                
                self.products = result
                
            case .failure(let error):
                
                NotificationBanner(title: R.string.error, subtitle: R.string.error_internet_connection, style: .danger).show()
                
                print(error)
            }
            
            self.isLoading = false
        }
    }
    
}
