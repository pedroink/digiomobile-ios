//
//  ActivityView.swift
//  digio
//
//  Created by MacBook Pro 2016 15" on 19/11/20.
//

import SwiftUI

struct ActivityView: View {
    
    @State private var animateStrokeStart = true
    @State private var animateStrokeEnd = false
    @State private var isRotating = true
    
    let color:Color!
    let size:CGFloat!
    
    var body: some View {
        ZStack {
                Circle()
                    .trim(from: animateStrokeStart ? 1/3 : 1/9, to: animateStrokeEnd ? 2/5 : 1)
                                .stroke(lineWidth: 3)
                    .frame(width: size, height: size)
                    .foregroundColor(color)
                    .rotationEffect(.degrees(isRotating ? 360 : 0))
                    .onAppear() {
                        
                        withAnimation(Animation.linear(duration: 1).repeatForever(autoreverses: false))
                        {
                            self.isRotating.toggle()
                        }
                        
                        withAnimation(Animation.linear(duration: 1).delay(0.5).repeatForever(autoreverses: true))
                        {
                            self.animateStrokeStart.toggle()
                        }
                        
                        withAnimation(Animation.linear(duration: 1).delay(1).repeatForever(autoreverses: true))
                                       {
                                           self.animateStrokeEnd.toggle()
                                       }
                        
                        
                }
        }
    }
}
