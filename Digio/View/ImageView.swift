//
//  ImageView.swift
//  digio
//
//  Created by MacBook Pro 2016 15" on 19/11/20.
//

import SwiftUI
import URLImage

struct ImageView: View {
    @State var image:UIImage = UIImage()
    var width:CGFloat
    var height:CGFloat
    var url:String

    init(withURL url:String, width:CGFloat, height:CGFloat) {
        self.url = url
        self.width = width
        self.height = height
    }

    var body: some View {
        
            ZStack {
                
                //ProgressView()
                
                URLImage(URL(string:url)!,
                content:  {
                    $0.image
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                    .clipped()
                })
                
                
            }
        
    }
}
