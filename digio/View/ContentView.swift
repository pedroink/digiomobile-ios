//
//  ContentView.swift
//  digio
//
//  Created by MacBook Pro 2016 15" on 19/11/20.
//

import SwiftUI
import URLImage

struct ContentView: View {
    
    @ObservedObject var productsVM = ProductsViewModel()
    
    
    var body: some View {
        
        ZStack (alignment: .top) {
        
            NavigationView{
                
                VStack{
                    
                    if(productsVM.isLoading){
                        ActivityView(color: R.color.white, size: 20).padding(.trailing)
                    }else{
                        
                        if (self.productsVM.products != nil) {
                            ScrollView{
                                
                                VStack (alignment: .leading){
                                    
                                    VStack{
                                        
                                        
                                        ScrollView(.horizontal, content: {
                                            HStack(spacing: 20) {
                                                ForEach(self.productsVM.products!.spotlight){ spotlight in
                                                    
                                                    ImageView(withURL:spotlight.bannerURL, width: 100, height: 100)
                                                        .cornerRadius(10)
                                                        .shadow(radius: 10)
                                                    
                                                    
                                                    
                                                }
                                                
                                            }.frame(height: 200).padding()
                                        })
                                        
                                    }
                                    
                                    
                                    VStack (alignment: .leading) {
                                    
                                        Text(self.productsVM.products!.cash.title).font(Font.system(size: 32, weight: .bold))
                                            .foregroundColor(R.color.blue)
                                        
                                        ImageView(withURL:self.productsVM.products!.cash.bannerURL, width: 100, height: 100)
                                            .cornerRadius(10)
                                            .shadow(radius: 10)
                                        
                                    }.padding()
                                
                                    
                                    VStack (alignment: .leading, spacing: 0) {
                                        
                                        Text(R.string.products).font(Font.system(size: 32, weight: .bold))
                                            .foregroundColor(R.color.blue).padding(.leading)
                                        
                                        ScrollView(.horizontal, content: {
                                            HStack(spacing: 20) {
                                                ForEach(self.productsVM.products!.products){ product in
                                                    
                                                    ImageView(withURL:product.imageURL, width: 100, height: 100)
                                                        .padding()
                                                        .background(
                                                            RoundedRectangle(cornerRadius: 25)
                                                                .fill(Color.white)
                                                                .shadow(color: R.color.shadow, radius: 10, x: 0, y: 2)).frame(height: 100)
                                                    
                                                    
                                                    
                                                }
                                                
                                            }.padding()
                                        })
                                    }
                                    
                                }
                                
                            }
                        }
                    }
                }
                .navigationBarTitle("Olá, Maria")
            }
        }.onAppear(){
            self.productsVM.fetch()
        }
    }
}

